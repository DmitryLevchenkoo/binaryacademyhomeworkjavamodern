package com.binary_studio.tree_max_depth;

import java.util.LinkedList;
import java.util.List;

public final class DepartmentMaxDepth {

	public static void main(String[] args) {
	}

	private DepartmentMaxDepth() {
	}

	public static Integer calculateMaxDepth(Department rootDepartment) {
		if (rootDepartment == null) {
			return 0;
		}
		Integer k = 0;
		List<Department> a = new LinkedList<>();
		a.add(rootDepartment);
		List<Department> b = new LinkedList<>();
		while (true) {

			for (int i = 0; i < a.size(); i++) {

				b.addAll(a.get(i).subDepartments);
			}

			for (int i = 0; i < b.size(); i++) {
				if (b.get(i) == null) {
					b.remove(i);
					i--;
				}
			}

			if (a.size() == 0) {
				return k;
			}
			k++;
			a = new LinkedList<>(b);
			b.clear();
		}
	}

}
