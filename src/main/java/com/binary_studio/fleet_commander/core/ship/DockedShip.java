package com.binary_studio.fleet_commander.core.ship;

import com.binary_studio.fleet_commander.core.common.PositiveInteger;
import com.binary_studio.fleet_commander.core.exceptions.InsufficientPowergridException;
import com.binary_studio.fleet_commander.core.exceptions.NotAllSubsystemsFitted;
import com.binary_studio.fleet_commander.core.ship.contract.ModularVessel;
import com.binary_studio.fleet_commander.core.ship.util.SystemPG;
import com.binary_studio.fleet_commander.core.subsystems.contract.AttackSubsystem;
import com.binary_studio.fleet_commander.core.subsystems.contract.DefenciveSubsystem;

public final class DockedShip implements ModularVessel {

	private String name;

	private PositiveInteger shieldHP;

	private PositiveInteger hullHP;

	private PositiveInteger powergridOutput;

	private PositiveInteger capacitorAmount;

	private PositiveInteger capacitorRechargeRate;

	private PositiveInteger speed;

	private PositiveInteger size;

	private SystemPG systemPG;

	public static DockedShip construct(String name, PositiveInteger shieldHP, PositiveInteger hullHP,
			PositiveInteger powergridOutput, PositiveInteger capacitorAmount, PositiveInteger capacitorRechargeRate,
			PositiveInteger speed, PositiveInteger size) {
		return new DockedShip(name, shieldHP, hullHP, powergridOutput, capacitorAmount, capacitorRechargeRate, speed,
				size);
	}

	public DockedShip(String name, PositiveInteger shieldHP, PositiveInteger hullHP, PositiveInteger powergridOutput,
			PositiveInteger capacitorAmount, PositiveInteger capacitorRechargeRate, PositiveInteger speed,
			PositiveInteger size) {
		this.name = name;
		this.shieldHP = shieldHP;
		this.hullHP = hullHP;
		this.powergridOutput = powergridOutput;
		this.capacitorAmount = capacitorAmount;
		this.capacitorRechargeRate = capacitorRechargeRate;
		this.speed = speed;
		this.size = size;
		this.systemPG = new SystemPG();
	}

	@Override
	public void fitAttackSubsystem(AttackSubsystem subsystem) throws InsufficientPowergridException {
		if (subsystem == null) {
			this.systemPG.setAttackSubsystem(null);
		}
		else {
			if (this.systemPG.generalSystemPG() + subsystem.getPowerGridConsumption().value() <= this.powergridOutput
					.value()) {
				this.systemPG.setAttackSubsystem(subsystem);
			}
			else {
				throw new InsufficientPowergridException(this.systemPG.generalSystemPG()
						+ subsystem.getPowerGridConsumption().value() - this.powergridOutput.value());
			}
		}
	}

	@Override
	public void fitDefensiveSubsystem(DefenciveSubsystem subsystem) throws InsufficientPowergridException {

		if (subsystem == null) {
			this.systemPG.setDefenciveSubsystem(null);
		}
		else {
			if (this.systemPG.generalSystemPG() + subsystem.getPowerGridConsumption().value() <= this.powergridOutput
					.value()) {
				this.systemPG.setDefenciveSubsystem(subsystem);
			}
			else {
				throw new InsufficientPowergridException(this.systemPG.generalSystemPG()
						+ subsystem.getPowerGridConsumption().value() - this.powergridOutput.value());
			}

		}

	}

	public CombatReadyShip undock() throws NotAllSubsystemsFitted {
		if (this.systemPG.getAttackSubsystem() == null && this.systemPG.getDefenciveSubsystem() == null) {
			throw NotAllSubsystemsFitted.bothMissing();
		}
		if (this.systemPG.getDefenciveSubsystem() == null) {
			throw NotAllSubsystemsFitted.defenciveMissing();
		}
		if (this.systemPG.getAttackSubsystem() == null) {
			throw NotAllSubsystemsFitted.attackMissing();
		}
		else {
			return new CombatReadyShip(this.name, this.shieldHP, this.hullHP, this.powergridOutput,
					this.capacitorAmount, this.capacitorRechargeRate, this.speed, this.size, this.systemPG);
		}
	}

}
