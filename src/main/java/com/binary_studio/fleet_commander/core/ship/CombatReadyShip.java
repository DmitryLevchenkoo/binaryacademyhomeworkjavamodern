package com.binary_studio.fleet_commander.core.ship;

import java.util.Optional;

import com.binary_studio.fleet_commander.core.actions.attack.AttackAction;
import com.binary_studio.fleet_commander.core.actions.defence.AttackResult;
import com.binary_studio.fleet_commander.core.actions.defence.RegenerateAction;
import com.binary_studio.fleet_commander.core.common.Attackable;
import com.binary_studio.fleet_commander.core.common.PositiveInteger;
import com.binary_studio.fleet_commander.core.ship.contract.CombatReadyVessel;
import com.binary_studio.fleet_commander.core.ship.util.SystemPG;

public final class CombatReadyShip implements CombatReadyVessel {

	private String name;

	private PositiveInteger shieldHP;

	private PositiveInteger hullHP;

	private PositiveInteger powergridOutput;

	private PositiveInteger capacitorAmount;

	private PositiveInteger capacitorRechargeRate;

	private PositiveInteger speed;

	private PositiveInteger size;

	private SystemPG systemPG;

	private Integer currentCapacity;

	private Integer currentshieldHP;

	private Integer currenthullHP;

	public CombatReadyShip(String name, PositiveInteger shieldHP, PositiveInteger hullHP,
			PositiveInteger powergridOutput, PositiveInteger capacitorAmount, PositiveInteger capacitorRechargeRate,
			PositiveInteger speed, PositiveInteger size, SystemPG systemPG) {
		this.name = name;
		this.shieldHP = shieldHP;
		this.hullHP = hullHP;
		this.powergridOutput = powergridOutput;
		this.capacitorAmount = capacitorAmount;
		this.capacitorRechargeRate = capacitorRechargeRate;
		this.speed = speed;
		this.size = size;
		this.systemPG = systemPG;
		this.currentCapacity = capacitorAmount.value();
		this.currentshieldHP = shieldHP.value();
		this.currenthullHP = hullHP.value();
	}

	@Override
	public void endTurn() {
		if (this.currentCapacity + this.capacitorRechargeRate.value() > this.capacitorAmount.value()) {
			this.currentCapacity = this.capacitorAmount.value();
		}
		else {
			this.currentCapacity += this.capacitorRechargeRate.value();
		}
	}

	@Override
	public void startTurn() {
	}

	@Override
	public String getName() {
		return this.name;
	}

	@Override
	public PositiveInteger getSize() {
		return this.size;
	}

	@Override
	public PositiveInteger getCurrentSpeed() {
		return this.speed;
	}

	@Override
	public Optional<AttackAction> attack(Attackable target) {
		Optional<AttackAction> optionalAttackAction = Optional.empty();
		if (this.currentCapacity >= this.systemPG.getAttackSubsystem().getCapacitorConsumption().value()) {
			this.currentCapacity -= this.systemPG.getAttackSubsystem().getCapacitorConsumption().value();
			optionalAttackAction = Optional.of(new AttackAction(this.systemPG.getAttackSubsystem().attack(target), this,
					target, this.systemPG.getAttackSubsystem()));
		}
		return optionalAttackAction;
	}

	@Override
	public AttackResult applyAttack(AttackAction attack) {
		AttackAction outatackaction = this.systemPG.getDefenciveSubsystem().reduceDamage(attack);
		AttackResult attackResult = new AttackResult.DamageRecived(outatackaction.weapon, outatackaction.damage,
				outatackaction.target);
		if (outatackaction.damage.value() < this.currentshieldHP) {
			this.currentshieldHP -= outatackaction.damage.value();
		}
		else {
			if (outatackaction.damage.value() < (this.currenthullHP + this.currentshieldHP)) {
				this.currenthullHP -= (outatackaction.damage.value() - this.currentshieldHP);
				this.currentshieldHP = 0;
			}
			else {
				attackResult = new AttackResult.Destroyed();
			}
		}
		return attackResult;
	}

	@Override
	public Optional<RegenerateAction> regenerate() {
		Optional<RegenerateAction> optionalRegenerateAction = Optional.empty();
		Integer regerateHullHP = 0;
		Integer regenarateShieldHP = 0;
		if (this.currentCapacity >= this.systemPG.getDefenciveSubsystem().getCapacitorConsumption().value()) {
			this.currentCapacity -= this.systemPG.getDefenciveSubsystem().getCapacitorConsumption().value();
			if (this.currenthullHP < this.hullHP.value()) {
				if (this.hullHP.value()
						- this.currenthullHP < this.systemPG.getDefenciveSubsystem().regenerate().hullHPRegenerated
								.value()) {
					regerateHullHP = this.hullHP.value() - this.currenthullHP;
				}
				else {
					regerateHullHP = this.systemPG.getDefenciveSubsystem().regenerate().hullHPRegenerated.value();
				}
			}
			if (this.currentshieldHP < this.shieldHP.value()) {
				if (this.shieldHP.value()
						- this.currentshieldHP < this.systemPG.getDefenciveSubsystem().regenerate().shieldHPRegenerated
								.value()) {
					regenarateShieldHP = this.shieldHP.value() - this.currentshieldHP;
				}
				else {
					regenarateShieldHP = this.systemPG.getDefenciveSubsystem().regenerate().shieldHPRegenerated.value();
				}
			}
			optionalRegenerateAction = Optional.of(
					new RegenerateAction(new PositiveInteger(regenarateShieldHP), new PositiveInteger(regerateHullHP)));
		}
		return optionalRegenerateAction;
	}

}
