package com.binary_studio.fleet_commander.core.ship.util;

import com.binary_studio.fleet_commander.core.subsystems.contract.AttackSubsystem;
import com.binary_studio.fleet_commander.core.subsystems.contract.DefenciveSubsystem;

public class SystemPG {

	private DefenciveSubsystem defenciveSubsystem;

	private AttackSubsystem attackSubsystem;

	public SystemPG() {
		this.defenciveSubsystem = null;
		this.attackSubsystem = null;
	}

	public DefenciveSubsystem getDefenciveSubsystem() {
		return this.defenciveSubsystem;
	}

	public void setDefenciveSubsystem(DefenciveSubsystem defenciveSubsystem) {
		this.defenciveSubsystem = defenciveSubsystem;
	}

	public AttackSubsystem getAttackSubsystem() {
		return this.attackSubsystem;
	}

	public void setAttackSubsystem(AttackSubsystem attackSubsystem) {
		this.attackSubsystem = attackSubsystem;
	}

	public Integer generalSystemPG() {
		Integer generalPG = 0;
		if (this.attackSubsystem != null) {
			generalPG += this.attackSubsystem.getPowerGridConsumption().value();
		}
		if (this.defenciveSubsystem != null) {
			generalPG += this.defenciveSubsystem.getPowerGridConsumption().value();
		}
		return generalPG;
	}

}
