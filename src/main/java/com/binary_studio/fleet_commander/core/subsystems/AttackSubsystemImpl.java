package com.binary_studio.fleet_commander.core.subsystems;

import com.binary_studio.fleet_commander.core.common.Attackable;
import com.binary_studio.fleet_commander.core.common.PositiveInteger;
import com.binary_studio.fleet_commander.core.subsystems.contract.AttackSubsystem;

public final class AttackSubsystemImpl implements AttackSubsystem {

	private String name;

	private PositiveInteger capacitorConsumption;

	private PositiveInteger powergridRequirments;

	private PositiveInteger baseDamage;

	private PositiveInteger optimalSpeed;

	private PositiveInteger optimalSize;

	public static AttackSubsystemImpl construct(String name, PositiveInteger powergridRequirments,
			PositiveInteger capacitorConsumption, PositiveInteger optimalSpeed, PositiveInteger optimalSize,
			PositiveInteger baseDamage) throws IllegalArgumentException {
		if (name != null && name.trim().equals("")) {
			throw new IllegalArgumentException("Name should be not null and not empty");
		}
		return new AttackSubsystemImpl(name, capacitorConsumption, powergridRequirments, baseDamage, optimalSize,
				optimalSpeed);
	}

	public AttackSubsystemImpl(String name, PositiveInteger capacitorConsumption, PositiveInteger powergridRequirments,
			PositiveInteger baseDamage, PositiveInteger optimalSize, PositiveInteger optimalSpeed) {
		this.name = name;
		this.capacitorConsumption = capacitorConsumption;
		this.powergridRequirments = powergridRequirments;
		this.baseDamage = baseDamage;
		this.optimalSize = optimalSize;
		this.optimalSpeed = optimalSpeed;
	}

	@Override
	public PositiveInteger getPowerGridConsumption() {
		return this.powergridRequirments;
	}

	@Override
	public PositiveInteger getCapacitorConsumption() {
		return this.capacitorConsumption;
	}

	@Override
	public PositiveInteger attack(Attackable target) {
		Double sizeReductionModifier;
		Double speedReductionModifier;
		if (target.getSize().value() >= this.optimalSize.value()) {
			sizeReductionModifier = 1.d;
		}
		else {
			sizeReductionModifier = 1.0 * target.getSize().value() / this.optimalSize.value();
		}
		if (target.getCurrentSpeed().value() <= this.optimalSpeed.value()) {
			speedReductionModifier = 1.d;
		}
		else {
			speedReductionModifier = 1.0 * this.optimalSpeed.value() / (2 * target.getCurrentSpeed().value());
		}
		return new PositiveInteger(
				(int) Math.ceil(this.baseDamage.value() * Math.min(sizeReductionModifier, speedReductionModifier)));
	}

	@Override
	public String getName() {
		return this.name;
	}

}
