package com.binary_studio.uniq_in_sorted_stream;

import java.util.stream.Stream;

public final class UniqueSortedStream {

	static long data = -1;

	public static void main(String[] args) {
	}

	private UniqueSortedStream() {
	}

	public static <T> Stream<Row<T>> uniqueRowsSortedByPK(Stream<Row<T>> stream) {
		UniqueSortedStream.data = -1;
		return stream.filter(n -> UniqueSortedStream(n.getPrimaryId()));
	}

	private static boolean UniqueSortedStream(long n) {
		if (UniqueSortedStream.data < n) {
			UniqueSortedStream.data = n;
			return true;
		}
		else {

			return false;
		}
	}

}
